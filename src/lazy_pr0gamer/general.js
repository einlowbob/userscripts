export function GM_getValue(key, defaultv) {
    let x = localStorage.getItem(key);
    if (x === null) {
        return defaultv;
    }
    return JSON.parse(x)
}

export function GM_setValue(key, value) {
    localStorage.setItem(key, JSON.stringify(value))
}


export function get_current_planet_id() {
    let planets = document.getElementById("planetSelector")
    let pname = planets.querySelector("option[value='" + planets.value + "']").innerText.split("[")[1]
    if (planets.querySelector("option[value='" + planets.value + "']").innerText.includes("(")) {
        pname = pname + "M"
    }
    return pname

}

export function get_sortet_planet_list(planets) {
    return planets.sort(comperator_planetids)

}

export function comperator_planetids(a, b) {
    let spta = a.split(":")
    let sptb = b.split(":")

    let ma = ("" + spta[2]).includes("M")
    if (ma) {
        spta[2] = spta[2].slice(0, -1)
    }
    let mb = ("" + sptb[2]).includes("M")
    if (mb) {
        sptb[2] = sptb[2].slice(0, -1)
    }
    spta[2] = spta[2].slice(0, -1)
    sptb[2] = sptb[2].slice(0, -1)


    if (parseInt(spta[0]) !== parseInt(sptb[0])) {
        if (parseInt(spta[0]) > parseInt(sptb[0])) {
            return 1
        }
        return -1
    }
    if (parseInt(spta[1]) !== parseInt(sptb[1])) {
        if (parseInt(spta[1]) > parseInt(sptb[1])) {
            return 1
        }
        return -1
    }

    if (parseInt(spta[2]) > parseInt(sptb[2])) {
        return 1
    }
    if (parseInt(spta[2]) < parseInt(sptb[2])) {
        return -1
    }
    if (ma) {
        return -1
    }
    return 1
}

export function pad(num, size) {
    num = num.toString();
    while (num.length < size) num = "0" + num;
    return num;
}