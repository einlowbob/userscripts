'use strict';

const {
  RaidParser
} = require('./raidParser')
const Chart = window['Chart']
let chartObj
const ep = new RaidParser()

function updateRaidDB() {
  const raids = ep.getMessages().map(m => ep.parse_text(m))
  const raidDB = GM_getValue("raids", [])
  const existingRaidsIds = raidDB.map(e => e.id + e.date.split("T")[0]) // match by id AND date in case game gets reset and ids are starting from 0 again
  let added = 0
  for (const raid of raids) {
    if (existingRaidsIds.indexOf(raid.id + raid.date.split("T")[0]) < 0) {
      raidDB.push(raid)
      added += 1
    }
  }
  GM_setValue("raids", raidDB)
  console.log(`Added ${added} new raids to database, now contains ${raidDB.length} entries`)
}

function createChart(type = "bar") {
  const backgroundPlugin = {
    id: 'custom_canvas_background_color',
    beforeDraw: (chart) => {
      const ctx = chart.canvas.getContext('2d')
      ctx.save()
      ctx.globalCompositeOperation = 'destination-over'
      ctx.fillStyle = "rgba(250, 250, 250, 0.9)"
      ctx.fillRect(0, 0, chart.width, chart.height)
      ctx.restore()
    }
  }
  const ctx = document.getElementById('charts').getContext('2d')
  //todo: darkmode
  chartObj = new Chart(ctx, {
    type,
    data: {},
    options: {
      scales: {
        x: {
          stacked: true
        },
        y: {
          stacked: true,
          beginAtZero: true
        }
      },
      responsive: true,
      maintainAspectRatio: false
    },
    plugins: [
      backgroundPlugin
    ]
  })
  return chartObj
}

function GM_addStyle(css) {
  const style = document.getElementById("GM_addStyleBy8626") || (function () {
    const style = document.createElement('style');
    style.type = 'text/css';
    style.id = "GM_addStyleBy8626";
    document.head.appendChild(style);
    return style;
  })();
  const sheet = style.sheet;
  sheet.insertRule(css, (sheet.rules || sheet.cssRules || []).length);
}

function addChart() {
  GM_addStyle(".raidstats-canvas { width: 100%; height: 400px; margin: 3px; }");
  const d = document.createElement("div")
  const c = document.createElement("canvas")
  d.appendChild(c)
  d.classList = "raidstats-canvas"
  c.id = "charts"
  const content = document.getElementsByTagName("content")[0]
  content.insertAdjacentElement("beforeend", d)

  function createOption(value, text) {
    const e = document.createElement("option")
    e.value = value
    e.text = text
    return e
  }
  const s = document.createElement("select")
  s.id = "chartSelect"
  s.addEventListener("change", (event) => draw(event.target.value))
  s.appendChild(createOption("dailyRes", "Daily Resources"))

  d.insertAdjacentElement("beforebegin", s)

  // create chart object
  chartObj = createChart("bar")

}

function getChartDataDailyRes() {
  const raidDB = GM_getValue("raids")
  const dates = raidDB.map(e => e.date.split("T")[0])

  const uniqueDates = [...new Set(dates)].sort()
  const resTypes = ["Metal", "Crystal", "Deuterium"]
  const datasets = []
  const raids = raidDB
  for (const resType of resTypes) {
    const data = []
    for (const date of uniqueDates) {
      let sum = 0
      for (const raid of raids.filter(e => e.date.split("T")[0] == date)){
        //console.log(raid[resType])
        sum += raid[resType]
        //console.log("raid: ", raid)
        //console.log("restype: ", resType)
      }
      data.push(sum)
    }
    let color = "#000"
    switch (resType) {
      case "Metal":
        color = "#666"
        break
      case "Crystal":
        color = "#d3d"
        break
      case "Deuterium":
        color = "#0d7"
    }
    datasets.push({
      label: resType,
      data,
      backgroundColor: [color]
    })
  }

  let labels = uniqueDates
  const data = {
    labels,
    datasets
  }
  return data
}

function draw(usecase) {
  // console.log("Drawing usecase: ", usecase)

  let data, chartType, isStacked = true
  switch (usecase) {
    case "dailyRes":
      data = getChartDataDailyRes()
      chartType = "bar"
      isStacked = false
      break
  }

  if (chartType != chartObj.type) {
    chartObj.destroy()
    chartObj = createChart(chartType)
  }
  chartObj.data = data
  chartObj.options.scales.x.stacked = isStacked
  chartObj.options.scales.y.stacked = isStacked
  chartObj.update()
}

if (ep.isRaidPage()) {
  updateRaidDB()
  addChart()
  draw(document.getElementById("chartSelect").value)
}